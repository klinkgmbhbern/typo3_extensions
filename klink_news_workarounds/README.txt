Workarounds for tx_news by Georg Ringer
***************************************

1.) Extend tx_news_domain_model_tag table and TCA for language support.
Tags can be translated in Typo3 Backend.

2.) Add language support for Previous/Next Links in Detail-mode.
These links do not work properly if you have translated news articles.
Next links to translated article which gets redirected to language your
are navigating in, so link lawys points to the same article.
These extension fixes the bug in the ViewHelper. Usage is very easy.
Set up the way tx_news uses your own Templates. In your template include the
ViewHelpers of this extension:
{namespace n=GeorgRinger\News\ViewHelpers}
{namespace k=KlinkGmbH\KlinkNewsWorkarounds\ViewHelpers}


and then use the k-ViewHelper instead od the n-ViewHelper:

<!-- simplePrevNextViewHelper -->
<k:simplePrevNext pidList="{newsItem.pid}" news="{newsItem}" as="paginated" sortField="datetime">
	<f:if condition="{paginated}">
		<ul class="prev-next">
			<f:if condition="{paginated.prev}">
				<li class="previous">
					<n:link newsItem="{paginated.prev}" settings="{settings}">
						<f:translate key="simplePrevNext-prev" />
					</n:link>
				</li>
			</f:if>
			<f:if condition="{paginated.next}">
				<li class="next">
					<n:link newsItem="{paginated.next}" settings="{settings}" class="next">
						<f:translate key="simplePrevNext-next" />
					</n:link>
				</li>
			</f:if>
		</ul>
	</f:if>
</k:simplePrevNext>

3.) ViewHelper for tag list that respects the language.
TagList somehow messes up depending on cache state. Sometimes displays correctly, sometimes duplicates entries
and displays translatet tags. This ViewHelper gets the active tags in the respective language.
Usage is very simple and basically just one modification to the original Tag/List.html template by Georg Ringer.
Add the ViewHelpers of this extension to the template and replace the <f:for ...> Fluid-Tag:
{namespace n=GeorgRinger\News\ViewHelpers}
{namespace k=KlinkGmbH\KlinkNewsWorkarounds\ViewHelpers}

<ul class="news-tags">
	<f:for each="{k:LanguageTagList(pidList: '{settings.startingpoint}')}" as="tag">
		<li>
			<f:if condition="{tag.uid} == {overwriteDemand.tags}">
				<f:then>
					<f:link.page title="{tag.title}" class="active" pageUid="{settings.listPid}" additionalParams="{tx_news_pi1:{overwriteDemand:{tags: tag}}}">
						{tag.title}
					</f:link.page>
				</f:then>
				<f:else>
					<f:link.page title="{tag.title}" pageUid="{settings.listPid}" additionalParams="{tx_news_pi1:{overwriteDemand:{tags: tag}}}">
						{tag.title}
					</f:link.page>
				</f:else>
			</f:if>

		</li>
	</f:for>
</ul>


