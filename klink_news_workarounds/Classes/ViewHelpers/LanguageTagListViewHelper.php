<?php

namespace KlinkGmbH\KlinkNewsWorkarounds\ViewHelpers;

	/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * ViewHelper to get array of tag-objects for chosen language.
 *
 * # Example: Basic example, simple modification to GeorgRinger Tag/List.html Template
 * <code>
 *
 *		<ul class="news-tags">
 *			<f:for each="{k:LanguageTagList(pidList: '{settings.startingpoint}')}" as="tag">
 *				<li>
 *					<f:if condition="{tag.uid} == {overwriteDemand.tags}">
 *						<f:then>
 *							<f:link.page title="{tag.title}" class="active" pageUid="{settings.listPid}" additionalParams="{tx_news_pi1:{overwriteDemand:{tags: tag}}}">
 *								{tag.title}
 *							</f:link.page>
 *						</f:then>
 *						<f:else>
 *							<f:link.page title="{tag.title}" pageUid="{settings.listPid}" additionalParams="{tx_news_pi1:{overwriteDemand:{tags: tag}}}">
 *								{tag.title}
 *							</f:link.page>
 *						</f:else>
 *					</f:if>
 *
 *				</li>
 *			</f:for>
 *		</ul>
 *
 *
 * </code>
 * <output>
 *  Unordered list with list item for each tag
 * </output>
 *
 */
class LanguageTagListViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/** @var \TYPO3\CMS\Core\Database\DatabaseConnection */
	protected $databaseConnection;

	/* @var $dataMapper \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper */
	protected $dataMapper;

	public function __construct() {
		$this->databaseConnection = $GLOBALS['TYPO3_DB'];
	}

	/**
	 * Inject the DataMapper
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper $dataMapper
	 * @return void
	 */
	public function injectDataMapper(\TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper $dataMapper) {
		$this->dataMapper = $dataMapper;
	}

	/**
	 * @param string $pidList pidList
	 * @param string $sortField
	 * @throws TYPO3\CMS\Fluid\Core\ViewHelper\Exception\InvalidVariableException
	 * @return string
	 */
	public function render($pidList, $sortField = 'title') {
		return $this->getTags($pidList, $sortField);
	}

	/**
	 * Returns where clause for the news table
	 *
	 * @return string
	 */
	protected function getEnableFieldsWhereClauseForTable() {
		$table = 'tx_news_domain_model_tag';
		if (is_object($GLOBALS['TSFE']) && is_object($GLOBALS['TSFE']->sys_page)) {
			$where = $GLOBALS['TSFE']->sys_page->enableFields($table);
			$where .= ' AND sys_language_uid=0';
			return $where;
		} elseif (is_object($GLOBALS['BE_USER'])) {
			return \TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause($table) .
			\TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields($table) .
			\TYPO3\CMS\Core\Resource\Utility\BackendUtility::getWorkspaceWhereClause($table);
		} elseif(TYPO3_MODE === 'BE' && TYPO3_cliMode === TRUE) {
			return '';
		}

		throw new \UnexpectedValueException('No TSFE for frontend and no BE_USER for Backend defined, please report the issue!');
	}

	/**
	 * @param $pidList
	 * @param $sortField
	 * @return array
	 */
	protected function getTags($pidList, $sortField) {
		$select = 'SELECT * ';
		$from = 'FROM tx_news_domain_model_tag ' ;
		$whereClause = 'WHERE tx_news_domain_model_tag.pid IN(' . $this->databaseConnection->cleanIntList($pidList) . ') ' . $this->getEnableFieldsWhereClauseForTable() . ' ';
		$order = 'ORDER BY ' .  $sortField . ' ASC';
		$query = $select . $from . $whereClause . $order;
		$res = $this->databaseConnection->sql_query($query);
		$out = array();
		$className = 'GeorgRinger\\News\\Domain\\Model\\Tag';
		while ($row = $this->databaseConnection->sql_fetch_assoc($res)) {
			$transRecord = $this->databaseConnection->exec_SELECTgetSingleRow('*', 'tx_news_domain_model_tag', 'l10n_parent=' . $row['uid'] . ' AND hidden=0 AND deleted=0 and sys_language_uid=' . $GLOBALS['TSFE']->sys_language_uid);
			if ($transRecord) {
				$row['title'] = $transRecord['title'];
			}
			#\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($transRecord);			
			$records = $this->dataMapper->map($className, array($row));
			$out[] = array_shift($records);
		}
		$this->databaseConnection->sql_free_result($res);
		return $out;
	}
}