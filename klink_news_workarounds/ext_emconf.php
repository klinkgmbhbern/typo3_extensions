<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "klink_news_workarounds"
 *
 * Auto generated by Extension Builder 2015-08-28
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Tx_news workarounds',
	'description' => 'Workarounds for tx_news extension mainly due to missing language support',
	'category' => 'misc',
	'author' => 'Lukas Kamber',
	'author_email' => 'lukas.kamber@klink.ch',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2',
			'news' => '3.2.0'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);