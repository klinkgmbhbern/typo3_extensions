<?php
class Tx_Klink_News_Workarounds_Hooks_TcaNews implements \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface {


        /**
         * Fügt tx_news zusätzliche TCA Anweisungen hinzu
         *
         * @return void
         */
        public function processData() {
			$GLOBALS['TCA']['tx_news_domain_model_tag']['ctrl']['languageField'] = 'sys_language_uid';
			$GLOBALS['TCA']['tx_news_domain_model_tag']['ctrl']['transOrigPointerField'] = 'l10n_parent';
			$GLOBALS['TCA']['tx_news_domain_model_tag']['ctrl']['transOrigDiffSourceField'] = 'l10n_diffsource';
			$column = array(
				'exclude' => 1,
				'label' => 'LLL:EXT:cms/locallang_ttc.xlf:sys_language_uid_formlabel',
				'config' => array(
					'type' => 'select',
					'foreign_table' => 'sys_language',
					'foreign_table_where' => 'ORDER BY sys_language.title',
					'items' => array(
						array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
						array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
					)
				)
			);
			$GLOBALS['TCA']['tx_news_domain_model_tag']['columns']['sys_language_uid'] = $column;

			$column = array(
				'displayCond' => 'FIELD:sys_language_uid:>:0',
				'exclude' => 1,
				'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
				'config' => array(
					'type' => 'select',
					'items' => array(
						array('', 0),
					),
					'foreign_table' => 'tx_news_domain_model_tag',
					'foreign_table_where' => 'AND tx_news_domain_model_tag.pid=###CURRENT_PID### AND tx_news_domain_model_tag.sys_language_uid IN (-1,0)',
				)
			);
			$GLOBALS['TCA']['tx_news_domain_model_tag']['columns']['l10n_diffsource'] = $column;

			$column = array(
				'config' => array(
					'type' => 'passthrough'
				)
			);
			$GLOBALS['TCA']['tx_news_domain_model_tag']['columns']['l10n_parent'] = $column;
			$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['tags']['config']['foreign_table_where'] ='AND tx_news_domain_model_tag.sys_language_uid=0 ORDER BY tx_news_domain_model_tag.title';
        }
}
?>
