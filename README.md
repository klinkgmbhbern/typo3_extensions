# Typo3 extensions #
Typo3 extension we develop for our websites that could be useful to others.  
For questions see [team on our homepage](http://klink.ch/team/) for e-mail adress - [www.klink.ch](www.klink.ch)  
*Lukas Kamber*  

## klink_news_workarounds ##
The very useful Typo3 [tx_news extension](http://typo3.org/extensions/repository/view/news) ( <= ver. 3.2.2) by Georg Ringer lacks translation support in some parts.  
This extension adds the required table fields and TCA configuration and ViewHelpers:  

* table fields and TCA to translate Tags in Typo3 backend
* ViewHelper to display Tag-list in current language
* ViewHelper that replaces buggy ViewHelper of tx_news for previous/next news article when having translated articles
